#!/bin/bash

# Prepare VPN
eval $(/sbin/ip r l m 0.0.0.0 | awk '{if($5!="tun0"){print "GW="$3"\nINT="$5; exit}}')
if [ -n "${GW-}" -a -n "${INT-}" ]; then
	/sbin/ip r a 192.168.1.0/24 via "$GW" dev "$INT"
fi

# Start VPN
echo "$OPENVPN_USER" >> /etc/openvpn/auth
echo "$OPENVPN_PASSWORD" >> /etc/openvpn/auth
/usr/sbin/openvpn --script-security 2 --config /etc/openvpn/openvpn.ovpn &
sleep 30
rm /etc/openvpn/auth
