FROM monkeysareevil/stretch
MAINTAINER MonkeysAreEvil

# Update this environment variable to the DNSCrypt servers you want to use
ENV SERVER_NAMES=[\'cs-de\',\'cs-de3\',\'cd-dk2\',\'cs-fi\',\'cs-nl\']
ENV DNSCRYPT_PROXY_VERSION=2.0.5

# Update and install deluge and vpn
RUN apt-get update -q --fix-missing
RUN apt-get -y upgrade
RUN apt-get -y install deluged deluge-web deluge-console openvpn
RUN echo "resolvconf resolvconf/linkify-resolvconf boolean false" | debconf-set-selections
RUN apt-get -y install resolvconf

# Prepare DNS proxy
RUN wget "https://github.com/jedisct1/dnscrypt-proxy/releases/download/${DNSCRYPT_PROXY_VERSION}/dnscrypt-proxy-linux_x86_64-${DNSCRYPT_PROXY_VERSION}.tar.gz" -O /tmp/dnscrypt-proxy.tar.gz
RUN tar xzvf /tmp/dnscrypt-proxy.tar.gz -C /tmp
# As per Gentoo/Funtoo tree
RUN cp /tmp/linux-x86_64/dnscrypt-proxy /usr/bin/dnscrypt-proxy
RUN cp /tmp/linux-x86_64/dnscrypt-proxy.service /lib/systemd/system/dnscrypt-proxy.service
RUN cp /tmp/linux-x86_64/dnscrypt-proxy.socket /lib/systemd/system/dnscrypt-proxy.socket
RUN mkdir -p /etc/dnscrypt-proxy
RUN cp /tmp/linux-x86_64/example-blacklist.txt /etc/dnscrypt-proxy/example-blacklist.txt
RUN cp /tmp/linux-x86_64/example-cloaking-rules.txt /etc/dnscrypt-proxy/example-cloaking-rules.txt
RUN cp /tmp/linux-x86_64/example-forwarding-rules.txt /etc/dnscrypt-proxy/example-forwarding-rules.txt
RUN cp /tmp/linux-x86_64/example-dnscrypt-proxy.toml /etc/dnscrypt-proxy/dnscrypt-proxy.toml
RUN sed -i "/#server_names/a server_names = ${SERVER_NAMES}" /etc/dnscrypt-proxy/dnscrypt-proxy.toml
# DNSCrypt-proxy only looks in cwd for the config file
WORKDIR "/etc/dnscrypt-proxy"
RUN /usr/bin/dnscrypt-proxy -service install
WORKDIR "/"

# Prepare supervisor
RUN apt-get install -y supervisor
ADD supervisord.conf /etc/supervisord.conf

# Prepare VPN
ADD openvpn.ovpn /etc/openvpn/openvpn.ovpn
ADD update-resolv-conf.sh /etc/openvpn/update-resolv-conf.sh
ADD start-openvpn.sh /usr/local/bin/start-openvpn.sh
RUN chmod +x /usr/local/bin/start-openvpn.sh
ENV OPENVPN_USER=""
ENV OPENVPN_PASSWORD=""

# Open ports
EXPOSE 8112
EXPOSE 58849

# Start
ADD start.sh /usr/local/bin/start.sh
RUN chmod +x /usr/local/bin/start.sh
CMD /usr/bin/supervisord
