# Description

A deluge container, with all traffic directed over an openvpn tunnel, and with DNS requests encrypted with dnscrypt-proxy. The deluge web interface is enabled.

# Install

1| Build a stretch base image.

```
emerge debootstrap
debootstrap stretch stretch > /dev/null
tar -C stretch -c . | docker import - monkeysareevil/stretch
```

2| Setup/buy a VPN. Stick the \*.ovpn file in this directory. Rename it to openvpn.ovpn. Enable DNS routing by sticking this onto the end of the file

```
dhcp-option DNS 127.0.0.1
script-security 2
up /etc/openvpn/update-resolv-conf.sh
down /etc/openvpn/update-resolv-conf.sh
```

and enable authentication from a file by adding

```
auth-user-pass /etc/openvpn/auth
```

i.e. just add `/etc/openvpn/auth`.

3| Build the image

```
docker build -t deluged .
```

Be sure to inspect the Dockerfile and update the DNSCrypt servers to the ones you want to use. Also note that you can change the dnscrypt-proxy version however I have only tested on 2.0.5. Note also that the VPN authentication is passed to the container via environment variables; if you're a loose cunt feel free to hard code these into the Dockerfile or service file. Otherwise make sure there's a `read` in your service file to prompt for authentication when starting the container.

4| Create a service file. `deluged` provides an example for openrc; adapt as required for other service managers. Note the `#DOWNLODS` environment variable --- update this to the required location on the host that downloads should be stored. Run the service, set the runlevel etc whaterver floats your boat. Feel free to check that your IP is correctly proxied using [torguard](https://torguard.net/checkmytorrentipaddress.php).

# Licence

GPL v3+
