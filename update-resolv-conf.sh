#!/usr/bin/env bash
#
# Forces openvpn to use local dns i.e. dnscrypt-proxy
# Based off jedisct1/dnscrypt-proxy
# but not shit and actually works
export PATH=$PATH:/sbin:/usr/sbin:/bin:/usr/bin
RESOLVCONF=$(type -p resolvconf)

case $script_type in

up)
  nameserver="nameserver 127.0.0.1"
  echo -n "$nameserver" | $RESOLVCONF -x -a "${dev}.inet"
  ;;
down)
  $RESOLVCONF -d "${dev}.inet"
  ;;
esac

# Workaround / jm@epiclabs.io 
# force exit with no errors. Due to an apparent conflict with the Network Manager
# $RESOLVCONF sometimes exits with error code 6 even though it has performed the
# action correctly and OpenVPN shuts down.
exit 0
