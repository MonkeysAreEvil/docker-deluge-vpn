#!/bin/bash

/usr/bin/dnscrypt-proxy -service start
echo dns-nameservers 127.0.0.1 >> /etc/network/interfaces
ifdown lo && ifup lo

# Prepare default config
deluged; sleep 5; pkill deluged

# Add user
echo 'guest:guest:5' >> /root/.config/deluge/auth

# Enable remote connection
deluge-console "config -s allow_remote True"

# Extra config
deluge-console "config -s stop_seed_at_ratio True"
